{
    "name": "VRACOOP - Processus Click&Collect",
    "summary": "VRACOOP - Processus Click&Collect",
    "version": "12.0.1.0.1",
    "development_status": "Beta",
    "author": "Le Filament",
    "maintainers": ["remi-filament"],
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "depends": [
        "website", "website_sale", "website_sale_delivery", "product_pack"
    ],
    "data": [
        'security/ir.model.access.csv',
        'datas/vracoop.time.csv',
        "views/assets.xml",
        "views/vracoop_retrait_views.xml",
        "views/vracoop_sale_retrait_views.xml",
        "views/vracoop_templates.xml",
        "views/sale_order.xml",
        "views/delivery_views.xml"
    ]
}
