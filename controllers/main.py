# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import http, _
from odoo.http import request
from odoo.addons.website_sale.controllers.main import WebsiteSale
from odoo.addons.website_sale_delivery.controllers.main import WebsiteSaleDelivery


class VracoopWebsiteSaleDelivery(WebsiteSale):

    @http.route(['/shop/update_retrait'], type='json', auth='public', methods=['POST'], website=True, csrf=False)
    def update_eshop_retrait(self, **post):
        results = {}
        if hasattr(self, '_update_website_sale_retrait'):
            results.update(self._update_website_sale_retrait(**post))

        return results

    @http.route(['/shop/check_type_carrier'], type='json', auth='public', methods=['POST'], website=True, csrf=False)
    def check_type_carrier(self, **post):
        results = {}
        if hasattr(self, '_check_carrier_type'):
            results.update(self._check_carrier_type(**post))

        return results

    def _get_shop_payment_values(self, order, **kwargs):
        values = super(VracoopWebsiteSaleDelivery, self)._get_shop_payment_values(order, **kwargs)
        points_retrait = request.env['vracoop.point.retrait'].sudo().search([('website_published', '=', True)])
        values['points_retrait'] = points_retrait
        return values

    def _update_website_sale_retrait(self, **post):
        order = request.website.sale_get_order()
        if order:
            if 'vracoop_point_retrait_id' in post:
                vracoop_point_retrait_id = int(post['vracoop_point_retrait_id'])
                order._check_retrait(force_retrait_id=vracoop_point_retrait_id)
            if 'hour_retrait' in post:
                hour_retrait = post['hour_retrait']
                order._update_day_hour(hour_retrait=hour_retrait)
                
        return self._update_website_sale_retrait_return(order, **post)

    def _update_website_sale_retrait_return(self, order, **post):
        status = False
        if 'vracoop_point_retrait_id' in post:
            vracoop_point_retrait_id = int(post['vracoop_point_retrait_id'])
        else:
            vracoop_point_retrait_id = ''
        if 'hour_retrait' in post:
            hour_retrait = post['hour_retrait']
            status = True
        else:
            hour_retrait = ''
        if order:
            return {'status': status,
                    'error_message': order.delivery_message,
                    'vracoop_point_retrait_id': vracoop_point_retrait_id,
                    'hour_retrait': hour_retrait
                    }
        return {}

    def _check_carrier_type(self, **post):
        order = request.website.sale_get_order()
        carrier_id = int(post['carrier_id'])
        carrier = request.env['delivery.carrier'].sudo().browse(carrier_id)
        point_retrait = carrier.point_retrait
        if order:
            return {'carrier_id': carrier_id,
                    'point_retrait': point_retrait
            }
        return {}
